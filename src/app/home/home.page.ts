import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  words = [];
  guessed: string;
  guess: string;
  hiddenWord: string;
  wordToBeGuessed: string;

  constructor(private toastController: ToastController) {}

  ngOnInit() {
    this.words.push('mobile');
    this.words.push('programming');
    this.words.push('object-orientation');

    const randomNumber = this.random();
    this.wordToBeGuessed = this.words[randomNumber];
    const star: string = '*';
    this.hiddenWord = star.repeat(this.wordToBeGuessed.length);
  }

  private random(): number {
    const rand = Math.floor(Math.random() * this.words.length);
    return rand;
  }

  private async check(event: any) {
    if (event.keyCode === 13 && this.guess.length > 0) {
      if (this.guess.length === 1) {
        if (this.guessed != undefined) {
          this.guessed = this.guessed + ',' + this.guess;
        }
        else {
          this.guessed = this.guess;
        }
          // Käydän sana läpi kirjain kirjaimelta
        for (let i = 0; i < this.wordToBeGuessed.length; i++) {
          if (this.wordToBeGuessed.substr(i, 1) === this.guess) {
            this.hiddenWord = this.hiddenWord.substring(0, i) +
            this.guess +
            this.hiddenWord.substring(i + 1);
          }
        }
      }

      //tänne koko sanan arvaus
      else {
        if (this.guess === this.wordToBeGuessed) {
          this.hiddenWord = this.wordToBeGuessed;
        }
      }

      if (this.hiddenWord === this.wordToBeGuessed) {
        const toast = await this.toastController.create({
          message: 'You have guessed the word!',
          position: 'top',
          duration: 3000
        });
        toast.present();
      }
      this.guess = '';
    }

  }

}
